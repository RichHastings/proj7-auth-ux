"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
app.config.from_object(__name__)
#app.secret_key = os.urandom(24)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

#db1 = client.tododb
db = client.brev
#db = client['brev']
#start = db["samplestart"]
#end = db["sampleend"]

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


@app.route('/todo', methods=["GET", "POST"])
def todo():
    _items = db.brev.find()
    items = [item for item in _items]
    db.brev.delete_many({})
    return render_template('todo.html', items=items)



@app.route('/new', methods=['POST', 'GET'])
def new():
    start=request.form.getlist("open")
    end=request.form.getlist("close")
    km=request.form.getlist("km")
    i=0
    for i in range(len(km)):
        if km[i]=="":
            break
        item_doc = {
            'km': km[i],
            'Open': start[i],
            'Close': end[i]
        }
        db.brev.insert_one(item_doc)

    #db.tododb.getName()
    return redirect(url_for('index'))

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 0, type=float)
    dist = request.args.get('dist', 0, type=float)
    date = request.args.get('date', 0, type=str)
    time = request.args.get('time', 0, type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    combtime = date + ' ' + time
    arrow_time = arrow.get(combtime, 'YYYY-MM-DD HH:mm')

    open_time = acp_times.open_time(km, dist, arrow_time)
    close_time = acp_times.close_time(km, dist, arrow_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
    #print("Opening for global access on port {}".format(CONFIG.PORT))
    #app.run(port=CONFIG.PORT, host="0.0.0.0")
