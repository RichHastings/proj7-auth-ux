from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)

import flask
import csv
import flask_restful
from io import StringIO
from flask import Flask, render_template, flash, redirect, url_for, request, Response
from password import hash_password, verify_password
from flask_restful import Resource, Api
from flask_httpauth import HTTPTokenAuth
from pymongo import MongoClient
import os
import json
from config import Config
from forms import LoginForm, RegForm, TokeForm
from testToken import generate_auth_token, verify_auth_token

# Instantiate the app
app = Flask(__name__)
api = Api(app)
auth = HTTPTokenAuth(scheme='Bearer')

app.config.from_object(Config)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brev
db2= client.users

class User(UserMixin):
    def __init__(self, name, id, password,active=True):
        self.name = name
        self.id = id
        self.active = active
        self.password = password

    def is_active(self):
        return self.active

USERS = {}
Tokens={}

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))



login_manager.setup_app(app)


@app.route("/")
@login_required
def index():
    #if check_token():
    return render_template("index.html",user=current_user)
    #else:
     #   Response("Access denied", 401)
      #  return url_for("login")

@app.route("/clear")
def clear():
    db2.users.delete_many({})
    return url_for("index")

@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("secret.html")


@api.representation('text/csv')
def output_csv(data, code, headers=None):
    ret=""
    with StringIO("") as csvfile:
        fieldnames = ['km', 'Close', "Open"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in data:
            writer.writerow(row)
        csvfile.seek(0)
        line = csvfile.readline()
        while line:
            print(line)
            ret+=line
            line = csvfile.readline()
    if headers=="open":
        ret=ret.replace("km,", "")
        ret=ret.replace("km", "")
        ret=ret.replace("Close,", "")
        ret=ret.replace("Close", "")
    elif headers=="close":
        ret=ret.replace("km,", "")
        ret=ret.replace("km", "")
        ret=ret.replace("Open,", '')
        ret=ret.replace("Open", '')
    return ret


@app.route("/api/register", methods=["GET", "POST"])
def register():
    form = RegForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        id = db.hundred.count({}) + 1
        if db2.users.find_one({"user": username}):
            flash('User Name {} already in use'.format(
                form.username.data))
        else:
            password = hash_password(password)
            tempclass = User(username, id, password)
            item_doc = {
                'user': username,
                'password': password,
                'id': id}
            USERS[id]= tempclass
            db2.users.insert_one(item_doc)
            return redirect(url_for("login"))
    return render_template('register.html',  title='Register', form=form)



@app.route("/api/token", methods=["GET", "POST"])
@login_required
def token():
    form=TokeForm()
    if form.validate_on_submit():
        token=generate_auth_token()
        Tokens[current_user.id]=token
        flash("Token created for {}".format(current_user.id))
    return render_template("token.html",  title='Token', form=form)

def check_token():
    if current_user.id in Tokens:
        if verify_auth_token(Tokens[current_user.id])=="Success":
            return True
    return False



@app.route("/api/login", methods=["GET", "POST"])
def login():
    USER_NAMES = dict((u.name, u) for u in USERS.values())
    form = LoginForm()
    if form.validate_on_submit():
        #flash('Login requested for user {}, remember_me={}'.format(
         #   form.username.data, form.remember_me.data))
        if db2.users.find_one({"user":form.username.data}):
            useInfo = db2.users.find_one({"user":form.username.data})
            password = useInfo["password"]
            remember = request.form.get("remember", "no") == "yes"
            if verify_password(form.password.data, password):
                user=User(useInfo["user"], useInfo["id"], useInfo["password"])
                login_user(user, remember=remember)
                return redirect(request.args.get("next") or url_for("login"))
        flash("Bad Password or User Name".format(form.password.data))
    return render_template('login.html',  title='Sign In', form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("login"))

class brevet(Resource):
    def __init__(self, **kwargs):
        self.csv=kwargs['csv']
        self.x = flask.request.args.get("top", type=int)

    def get(self):
        output=[]
        if self.x==None:
            _items = db.brev.find()
        else:
            _items = db.brev.find().limit(self.x)
        items = [item for item in _items]
        for item in items:
            item.pop('_id')
            output.append(item)
        if self.csv:
            return  output_csv(items, 200, "brevet")
        ret=json.dumps(str(output))
        ##flask.render_template('index.html', response=ret)
        return ret


class listOpenOnly(Resource):
    def __init__(self, **kwargs):
        self.csv=kwargs['csv']
        self.x=flask.request.args.get("top", type=int)

    def get(self):
        output=[]
        if self.x == None:
            _items = db.brev.find()
        else:
            _items = db.brev.find().limit(self.x)
        items = [item for item in _items]
        for item in items:
            item.pop('_id')
            output.append(item["Open"])
        if self.csv:
            return output_csv(items, 200, "open")
        ret = json.dumps(str(output))
        return ret



class listCloseOnly(Resource):
    def __init__(self, **kwargs):
        self.csv=kwargs['csv']
        self.x=flask.request.args.get("top", type=int)

    def get(self):
        output=[]
        if self.x == None:
            _items = db.brev.find()
        else:
            _items = _items = db.brev.find().limit(self.x)
        items = [item for item in _items]
        for item in items:
            item.pop('_id')
            output.append(item['Close'])
        if self.csv:
            output= output_csv(items, 200, "close")
        ret = json.dumps(str(output))
        return ret

# Create routes
# Another way, without decorators


api.add_resource(brevet, "/listAll/",  endpoint='listAll', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/json/",  endpoint='listAll-json',resource_class_kwargs={'csv': False} )
api.add_resource(listOpenOnly, '/listOpenOnly/', endpoint="listOpenOnly", resource_class_kwargs={'csv': False})
api.add_resource(listOpenOnly,"/listOpenOnly/json/", endpoint="listOpenOnly-json", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly, '/listCloseOnly/', endpoint= "listCloseOnly", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly,"/listCloseOnly/json/", endpoint='ListCloseOnly-json', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/csv/", resource_class_kwargs={'csv': True})
api.add_resource(listOpenOnly, "/listOpenOnly/csv/", resource_class_kwargs={'csv': True})
api.add_resource(listCloseOnly, "/listCloseOnly/csv/",resource_class_kwargs={'csv': True})
api.add_resource(brevet, "/listAll/?top={x}",  endpoint='listall-count', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/json/?top={x}",  endpoint='listAll-json-count',resource_class_kwargs={'csv': False} )
api.add_resource(listOpenOnly, '/listOpenOnly/?top={x}', endpoint="listOpenOnly-count", resource_class_kwargs={'csv': False})
api.add_resource(listOpenOnly,"/listOpenOnly/json/?top={x}", endpoint="listOpenOnly-json-count", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly, '/listCloseOnly/?top={x}', endpoint= "listCloseOnly-count", resource_class_kwargs={'csv': False})
api.add_resource(listCloseOnly,"/listCloseOnly/json/?top={x}", endpoint='ListCloseOnly-json-count', resource_class_kwargs={'csv': False})
api.add_resource(brevet, "/listAll/csv/?top={x}", endpoint="listall-csv-count", resource_class_kwargs={'csv': True})
api.add_resource(listOpenOnly, "/listOpenOnly/csv/?top={x}",endpoint="listopenonly-csv-count", resource_class_kwargs={'csv': True})
api.add_resource(listCloseOnly, "/listCloseOnly/csv/?top={x}",endpoint="liscloseone-csv-count",resource_class_kwargs={'csv': True})
#api.add_resource(login, "/login", endpoint= 'login')
#api.add_resource(login, "/register", endpoint= 'register')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
